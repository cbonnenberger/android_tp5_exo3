package com.example.cbonnenberger.tp5_exo3;

import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView liste ;
    FloatingActionButton addContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        liste = (ListView) findViewById(R.id.main_activity_listview);
        addContact = (FloatingActionButton) findViewById(R.id.main_activity_floatingActionButton);

        ContactSingleton.getInstance().addContact(new Contact("Camille Bonnenberger", "camille.bonnenberger@hotmail.fr","femme","20 rue Gambetta", "O673467720"));
        ContactSingleton.getInstance().addContact(new Contact("Axel Turlier", "axel.turlier@outlook.fr","homme","20 rue Gambetta", "0629190444"));

        ContactAdapter adapter = new ContactAdapter(ContactSingleton.getInstance().listeContact, this);
        ContactSingleton.getInstance().addObservateur(adapter);

        liste.setAdapter(adapter);




        addContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ContactFormulaire.class);
                startActivity(intent);
            }
        });






    }
}


