package com.example.cbonnenberger.tp5_exo3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class ContactFormulaire extends AppCompatActivity {

    EditText nom, mail, adresse, numTel;
    Spinner sexe;
    Button valider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_formulaire);


        nom = (EditText) findViewById(R.id.contact_formulaire_nom_edit_text);
        mail = (EditText) findViewById(R.id.contact_formulaire_mail_edit_text);
        adresse = (EditText) findViewById(R.id.contact_formulaire_adresse_edit_text);
        numTel = (EditText) findViewById(R.id.contact_formulaire_num_tel_edit_text);
        sexe = (Spinner) findViewById(R.id.contact_formulaire_sexe_spinner);
        valider = (Button) findViewById(R.id.contact_valider_button);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.sexe, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        sexe.setAdapter(adapter);

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!nom.getText().toString().isEmpty() && !numTel.getText().toString().isEmpty()) {
                    Contact contact = new Contact(nom.getText().toString(), numTel.getText().toString(), mail.getText().toString(), adresse.getText().toString(), sexe.getSelectedItem().toString());
                    ContactSingleton.getInstance().addContact(contact);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Veuillez remplir au moins les champs \"Numéro de téléphone\" et \"Nom\"",
                            Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

    }
}
