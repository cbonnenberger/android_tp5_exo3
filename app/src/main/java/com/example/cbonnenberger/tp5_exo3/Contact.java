package com.example.cbonnenberger.tp5_exo3;

/**
 * Created by cbonnenberger on 05/03/2017.
 */

public class Contact {
    private String nom, mail, sexe, adresse, numTel;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }





    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getNumTel() {
        return numTel;
    }

    public void setNumTel(String numTel) {
        this.numTel = numTel;
    }

    public Contact(String nom, String mail, String sexe, String adresse, String numTel) {
        this.nom = nom;
        this.mail = mail;
        this.sexe = sexe;
        this.adresse = adresse;
        this.numTel = numTel;
    }
}
