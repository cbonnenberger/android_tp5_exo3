package com.example.cbonnenberger.tp5_exo3;

import java.util.ArrayList;

/**
 * Created by cbonnenberger on 05/03/2017.
 */
public class ContactSingleton {

    ArrayList<Contact> listeContact;
    ArrayList<Observateur> observateurArrayList;

    private static ContactSingleton ourInstance = new ContactSingleton();

    public static ContactSingleton getInstance() {
        return ourInstance;
    }

    private ContactSingleton() {
        listeContact = new ArrayList<>();
        observateurArrayList = new ArrayList<>();
    }

    public void addContact(Contact c){
        listeContact.add(c);
        notifyObservateurs();
    }

    public void removeContact(Contact c){
        listeContact.remove(c);
        notifyObservateurs();
    }

    public void addObservateur(Observateur o){
        observateurArrayList.add(o);
    }

    public void removeObservateur(Observateur o){
        observateurArrayList.remove(o);
    }

    private void notifyObservateurs(){
        for(Observateur o : observateurArrayList){
            o.notifyContactListChanged();
        }
    }



}
