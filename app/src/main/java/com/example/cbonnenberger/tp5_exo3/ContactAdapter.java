package com.example.cbonnenberger.tp5_exo3;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.EmptyStackException;

/**
 * Created by cbonnenberger on 05/03/2017.
 */

public class ContactAdapter extends BaseAdapter implements Observateur{

    ArrayList<Contact> contactList;
    final int layout_id = R.layout.main_activity_listview_cell;
    LayoutInflater layoutInflater;
    Context context;

    public ContactAdapter(ArrayList<Contact> contactList, Context context) {
        this.contactList = contactList;
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return contactList.size();
    }

    @Override
    public Contact getItem(int position) {
        return contactList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Contact contact = getItem(position);

        LinearLayout linearLayout;
        if(convertView == null){
            linearLayout = (LinearLayout)layoutInflater.inflate(layout_id, parent, false);
        }else{
            linearLayout = (LinearLayout)convertView;
        }

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { // affichage apperçu du contacte
                Toast toast = Toast.makeText(context, contact.getNom() + " \n" + contact.getNumTel() +
                " \n" +  contact.getAdresse() +" \n"+ contact.getMail()+" \n "+ contact.getSexe(),
                        Toast.LENGTH_LONG);
                toast.show();
            }
        });


        linearLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ContactSingleton.getInstance().removeContact(contact);
                return true;
            }
        });



        ImageView contactPhoto = (ImageView)linearLayout.findViewById(R.id.main_activity_listview_cell_contact_imageView);
        TextView nomContact = (TextView)linearLayout.findViewById(R.id.main_activity_listview_cell_contact_name_textView);
        TextView numTel = (TextView)linearLayout.findViewById(R.id.main_activity_listview_cell_contact_number_textView);
        ImageView tel = (ImageView)linearLayout.findViewById(R.id.main_activity_listview_cell_phone_imageView);

        nomContact.setText(contact.getNom());
        numTel.setText(contact.getNumTel());

        // Quand on clique sur le petit téléphone on peut appeller le contact
        tel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+contact.getNumTel()));
                context.startActivity(intent);
            }
        });

        return linearLayout;
    }

    @Override
    public void notifyContactListChanged() {
        notifyDataSetChanged();
    }


}
